<?php

namespace App\Http\Controllers\Establishment;

use App\Http\Controllers\Controller;
use App\Models\Establishment;
use Illuminate\Http\Request;

class EstablishmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function all()
    {
        $establishment = Establishment::with('establishment_categories')->get();

        return response($establishment, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            Establishment::create([
                'name' => $request->name,
                'description' => $request->description,
                'establishment_category_id' => $request->establishment_category_id,
                'user_id' => $request->user()->id,
                'cep' => $request->cep,
                'logradouro' => $request->logradouro,
                'complemento' => $request->complemento,
                'bairro' => $request->bairro,
                'localidade' => $request->localidade,
                'uf' => $request->uf
            ]);

            return response([
                'message' => 'Estabelecimento criado com sucesso!'
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $product = Establishment::find($id);

            $product->update($request->all());

            return response(['message' => 'Estabelecimento atualizado com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            Establishment::destroy($id);

            return response(['message' => 'Estabelecimento excluido com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
