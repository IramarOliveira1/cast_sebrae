<?php

namespace App\Http\Controllers\EstablishmentCategory;

use App\Http\Controllers\Controller;
use App\Models\EstablishmentCategory;
use Illuminate\Http\Request;

class EstablishmentCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function all()
    {
        $establishmentCategory = EstablishmentCategory::all();

        return response($establishmentCategory, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            EstablishmentCategory::create([
                'name' => $request->name
            ]);
            return response([
                'message' => 'Categoria do estabelecimento criado com sucesso'
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $product = EstablishmentCategory::find($id);

            $product->update($request->all());

            return response(['message' => 'Categoria estabelecimento atualizado com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            EstablishmentCategory::destroy($id);

            return response(['message' => 'Categoria estabelecimento excluido com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
