<?php

namespace App\Http\Controllers\ProductCategory;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function all()
    {
        $ProductCategory = ProductCategory::all();

        return response($ProductCategory, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            ProductCategory::create([
                'name' => $request->name
            ]);
            return response([
                'message' => 'Categoria do produto criada com sucesso!'
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $product = ProductCategory::find($id);

            $product->update($request->all());

            return response(['message' => 'Categoria produto atualizado com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            ProductCategory::destroy($id);

            return response(['message' => 'Categoria produto excluido com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
