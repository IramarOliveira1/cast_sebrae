<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Establishment;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function getOrderClient(Request $request)
    {
        $order = Order::with(['users', 'products', 'establishments'])
            ->where('user_id', '=', $request->user()->id)
            ->groupBy('number_order')
            ->get();

        return response($order, 200);
    }

    public function getOrderEstablishments(Request $request)
    {
        $id = Establishment::where('user_id', '=', $request->user()->id)->first();

        $order = Order::with(['users', 'products', 'establishments'])
            ->where('user_id', '=', $request->user()->id)
            ->where('establishment_id', '=', $id->id)
            ->groupBy('number_order')
            ->get();

        return response($order, 200);
    }

    public function setOrder(Request $request)
    {
        try {
            $total = 0;
            $lastKey = array_key_last($request->order);

            foreach ($request->order as $key => $item) {
                $calculate = $item['amount'] * $item['price'];
                $total = $calculate + $total;

                if ($lastKey === $key) {
                    if ($total > $request->user()->balance) {
                        return throw new \Exception('Saldo insuficiente!');
                    }

                    $number_order = Carbon::now()->timestamp;

                    for ($i = 0; $i < count($request->order); $i++) {
                        $order =  Order::create([
                            'amount' => $request->order[$i]['amount'],
                            'user_id' => $request->user()->id,
                            'establishment_id' => $request->order[$i]['establishment_id'],
                            'product_id' => $request->order[$i]['id'],
                            'number_order' => $number_order
                        ]);

                        $id_order = Order::where('number_order', '=', $order->number_order)->first();

                        $id_order->products()->attach(
                            $request->user()->id,
                            [
                                'user_id' => $request->user()->id,
                                'order_id' => $id_order->id,
                                'product_id' => $order->product_id,
                                'establishment_id' => $order->establishment_id
                            ]
                        );
                    }
                }
            }

            $updateBalance = User::find($request->user()->id);

            $decreaseBalance = $updateBalance->balance - $total;

            $updateBalance->update(['balance' => $decreaseBalance]);

            return response(['message' => 'Pedido criado com sucesso!', 'user' => $updateBalance], 201);
        } catch (\Exception $e) {
            return response(['message' => $e->getMessage()], 400);
        }
    }
}
