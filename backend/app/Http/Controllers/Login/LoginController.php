<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function Login(Request $request)
    {
        if (Auth::attempt($request->all())) {

            $token = $request->user()->createToken('MyApp');

            return response([
                'user' => $request->user(),
                'token' => $token->plainTextToken
            ], 200);
        }

        return response([
            'message' => 'Login ou Senha invalidos!'
        ], 401);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return response(['message' => 'Cliente deslogado com sucesso!'], 200);
    }
}
