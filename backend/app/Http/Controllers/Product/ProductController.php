<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function all()
    {
        $product = Product::with(['establishments', 'product_categories'])->paginate(15);

        return response($product, 200);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            Product::create([
                'name' => $request->name,
                'price' => $request->price,
                'establishment_id' => $request->establishment_id,
                'product_category_id' => $request->product_category_id
            ]);
            return response(['message' => 'Produto criado com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function filterProduct(Request $request)
    {
        $filter = Product::query()
            ->where('products.name', 'LIKE', '%' . $request->query('search') . '%')
            ->join('product_categories', 'product_category_id', '=', 'product_categories.id')
            ->orWhere('product_categories.name', 'LIKE', '%' . $request->query('search') . '%')
            ->join('establishments', 'establishment_id', '=', 'establishments.id')
            ->orWhere('establishments.name', 'LIKE', '%' . $request->query('search') . '%')
            ->selectRaw(
                'establishments.name AS establishments_name, establishments.id AS establishment_id ,
                product_categories.name AS product_categories_name , product_categories.id AS product_category_id,
                products.name AS products_name, products.id AS products_id'
            )->get();

        return response($filter, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $product = Product::find($id);

            $product->update($request->all());

            return response(['message' => 'Produto atualizado com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            Product::destroy($id);

            return response(['message' => 'Produto excluido com sucesso!'], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
