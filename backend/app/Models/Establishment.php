<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'establishment_category_id',
        'user_id',
        'cep',
        'logradouro',
        'complemento',
        'bairro',
        'localidade',
        'uf'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];


    public function establishment_categories()
    {
        return $this->belongsTo(EstablishmentCategory::class, 'establishment_category_id', 'id');
    }
}
