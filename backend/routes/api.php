<?php

use App\Http\Controllers\Establishment\EstablishmentController;
use App\Http\Controllers\EstablishmentCategory\EstablishmentCategoryController;
use App\Http\Controllers\Login\LoginController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\ProductCategory\ProductCategoryController;
use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/logout', [LoginController::class, 'logout']);

    Route::get('/order-client', [OrderController::class, 'getOrderClient']);
    Route::get('/order-establishments', [OrderController::class, 'getOrderEstablishments']);
    Route::post('/order', [OrderController::class, 'setOrder']);

    Route::post('/establishment-category', [EstablishmentCategoryController::class, 'store']);
    Route::get('/establishment-category', [EstablishmentCategoryController::class, 'all']);
    Route::put('/establishment-category/{id}', [EstablishmentCategoryController::class, 'update']);
    Route::delete('/establishment-category/{id}', [EstablishmentCategoryController::class, 'destroy']);

    Route::post('/establishment', [EstablishmentController::class, 'store']);
    Route::get('/establishment', [EstablishmentController::class, 'all']);
    Route::put('/establishment/{id}', [EstablishmentController::class, 'update']);
    Route::delete('/establishment/{id}', [EstablishmentController::class, 'destroy']);

    Route::post('/product-category', [ProductCategoryController::class, 'store']);
    Route::get('/product-category', [ProductCategoryController::class, 'all']);
    Route::put('/product-category/{id}', [ProductCategoryController::class, 'update']);
    Route::delete('/product-category/{id}', [ProductCategoryController::class, 'destroy']);

    Route::get('/filter', [ProductController::class, 'filterProduct']);
    Route::post('/product', [ProductController::class, 'store']);
    Route::get('/product', [ProductController::class, 'all']);
    Route::put('/product/{id}', [ProductController::class, 'update']);
    Route::delete('/product/{id}', [ProductController::class, 'destroy']);

    Route::get('/user', [UserController::class, 'show']);
});


Route::post('/user', [UserController::class, 'store']);

Route::post('/login', [LoginController::class, 'login']);
