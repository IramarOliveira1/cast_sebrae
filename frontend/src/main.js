import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'

import './style.css';

import router from './router/index';

const pinia = createPinia()

import 'ant-design-vue/dist/antd.css';
import antD from 'ant-design-vue';

import axios from './services/api';

const app = createApp(App);

app.config.globalProperties.$axios = { ...axios }

app.use(router).use(antD).use(pinia).mount('#app');
