import { createRouter, createWebHistory } from 'vue-router';

const routes = [
    {
        name: 'login',
        path: '/login',
        component: () => import('../pages/login/login.vue'),
        meta: { RequiresGuest: true },
    },
    {
        path: '/user',
        component: () => import('../pages/user/user.vue')
    },
    {
        name: 'redirect',
        path: '/redirect',
        component: () => import('../pages/dashboard/redirect.vue'),
        meta: { RequiresAuth: true },
        children: [
            {
                name: 'dashboard',
                path: '/dashboard',
                component: () => import('../pages/dashboard/dashboard.vue'),
                meta: { RequiresAuth: true },
            },
            {
                path: '/criar-categoria-produto',
                component: () => import('../pages/product/categoryProduct.vue'),
                meta: { RequiresAuth: true },
            },
            {
                path: '/criar-produto',
                component: () => import('../pages/product/product.vue'),
                meta: { RequiresAuth: true },
            },
            {
                path: '/criar-categoria-estabelecimento',
                component: () => import('../pages/establishment/categoryEstablishment.vue'),
                meta: { RequiresAuth: true },
            },
            {
                path: '/criar-estabelecimento',
                component: () => import('../pages/establishment/establishment.vue'),
                meta: { RequiresAuth: true },
            },
            {
                path: '/pedido-cliente',
                component: () => import('../pages/order/order.vue'),
                meta: { RequiresAuth: true },
            },
            {
                path: '/pedido-estabelecimento',
                component: () => import('../pages/order/orderEstablishment.vue'),
                meta: { RequiresAuth: true },
            },
        ]
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})


router.beforeEach((to, from) => {
    const authenticated = localStorage.getItem('token');

    if (to.meta.RequiresGuest && authenticated) {
        return {
            name: 'dashboard'
        };
    } else if (to.meta.RequiresAuth && !authenticated) {
        return {
            name: 'login'
        };
    }
})

export default router;