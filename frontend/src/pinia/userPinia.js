import { defineStore } from 'pinia'

export const userStore = defineStore('storeId', {
    state: () => {
        return {
            user: {}
        }
    },
})